import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int _current = 0;
  Future<dynamic> _future = getImageList();

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
            FutureBuilder(
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (snapshot.hasData) {
                    List<dynamic> list = snapshot.data;
                    double carouselWidth = 1280;
                    double itemWidth = 640;
                    double itemHeight = 480;
                    return Container(
                      width: carouselWidth,
                      child: Column(children: [
                        CarouselSlider(
                            options: CarouselOptions(
                                height: itemHeight,
                                disableCenter: true,
                                enlargeCenterPage: false,
                                viewportFraction: itemWidth / carouselWidth,
                                onPageChanged: (index, reason) {
                                  setState(() {
                                    _current = index;
                                  });
                                }),
                            items: list
                                .map((e) => Container(
                                      child: Center(
                                          child: Image.network(e,
                                              fit: BoxFit.cover,
                                              width: itemWidth)),
                                    ))
                                .toList()),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: list.map((url) {
                            int index = list.indexOf(url);
                            return Container(
                              width: 8.0,
                              height: 8.0,
                              margin: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 2.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _current == index
                                    ? Color.fromRGBO(0, 0, 0, 0.9)
                                    : Color.fromRGBO(0, 0, 0, 0.4),
                              ),
                            );
                          }).toList(),
                        ),
                      ]),
                    );
                  } else {
                    return Text("Downloading...");
                  }
                },
                future: _future),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

Future<dynamic> getImageList() async {
  String url = 'http://localhost:4041';
  try {
    var response = await http.get(url);
    Map<String, dynamic> jsonResponse = convert.jsonDecode(response.body);
    return jsonResponse['images'];
  } catch (e) {
    return null;
  }
}
